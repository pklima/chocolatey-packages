﻿$ErrorActionPreference = 'Stop';

$packageArgs = @{
  url           = 'https://github.com/premake/premake-core/releases/download/v5.0.0-beta1/premake-5.0.0-beta1-windows.zip'
  checksum      = 'D8F958162B202FE88A2FA1897D3C5342DD137F19D1265DE8802880126A808A65'
  checksumType  = 'sha256'

  packageName   = $env:ChocolateyPackageName
  unzipLocation = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
}

Install-ChocolateyZipPackage @packageArgs
